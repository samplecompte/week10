#Sam LeCompte
#11/16/15
# Unique words


gettysburgFile = open("gettysburg.txt" , 'r')

txt = gettysburgFile.read()
gettysburgFile.close()

words = txt.split()

uniqueWords = set(words)

print("These are the unique words in your file: ")
for words in uniqueWords:
    print(words)


unique_dictionary = {}

for word in words:
    if word in unique_dictionary:
        unique_dictionary[word] += 1
    else:
        unique_dictionary[word] = 1 

