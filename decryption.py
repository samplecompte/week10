#Sam LeCompte
#11/13/15
#Decryption

codes = {'A' : '1', 'a' : '!','B' : '2', 'b': '@', 'C': '3', 'c' : '#', 'D' : '4', 'd' :'$', 'E' : '5',
         'e' : '%', 'F' : '6', 'f' : '^', 'G' : '7', 'g': '&', 'H' : '8', 'h' : '*', 'I': '9', 'i' : '(',
         'J' : '0', 'j' : ')', 'K': '-', 'k': '_', 'L' : '=', 'l' : '+', 'M' : '\\', 'm' : '|', 'N' : ';',
         'n' : ';' , 'O' : '\'', 'o' : '"', 'P' : ',', 'p' : '<', 'Q': '.', 'q' : '>', 'R' : '/',
         'r' : '?',   'S': '`', 's' : '~', 'T' : 'q', 't' : 'Q', 'U': 'z', 'u' : 'Z', 'V' : 'w', 'v' : 'W',
         'W' : 'r', 'w' : 'R', 'X' : 'g', 'x' : 'G', 'Y' : 'h', 'y' : 'H', 'Z' : 'j', 'z' : 'J', ' ' : 'A',
         ',' : 'T', '?' : 'Y' , '\n' : 'G', "'" : 'U', '.' : 'V'}


decode = dict(zip(codes.values(), codes ))

encryptFile = open("encryptTxt.txt" , 'r')

txt = encryptFile.read()
encryptFile.close()




encryptString = ""

txtChar = list(txt)


for x in txtChar:
    char = decode[x]
    encryptString += char 



print(encryptString)
